chrome.runtime.onInstalled.addListener(() => {
  chrome.contextMenus.create({
    id: "exbirdl_image",
    title: "Save image with Exbirdl",
    contexts: ["image"],
    documentUrlPatterns: ["https://x.com/*", "https://twitter.com/*"],
  });
});

chrome.contextMenus.onClicked.addListener((item, tab) => {
  if (item.menuItemId == "exbirdl_image") {
    const imagePostUrl = item.linkUrl ? item.linkUrl : item.pageUrl;

    const fileNameNoExt = new URL(imagePostUrl).pathname.substring(1).replace(/\//g, " ");
    let imageSrcUrl = new URL(item.srcUrl);
    imageSrcUrl.searchParams.set("name", "large")

    downloadPic(imageSrcUrl.href, fileNameNoExt)
  }
});

function downloadPic(imagesrc, filename) {
  const url = imagesrc;
  
  fetch(url)
    .then((response) => {
      const filextension = response.headers.get("Content-Type").split("/")[1]
      const fileName = `@${filename}.${filextension}`

      chrome.downloads.download({
        url: url,
        filename: fileName,
      },
      (dlid) => console.log(`Download started with ID ${dlid}`));
    })
    .catch((error) => console.error("Error on downloading image:", error));
}
